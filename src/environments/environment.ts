// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: 'AIzaSyDXwOQVAe0Op5VjvGbMPp2FMIGOK35CGYs',
    authDomain: 'codetut-io.firebaseapp.com',
    databaseURL: 'https://codetut-io.firebaseio.com',
    projectId: 'codetut-io',
    storageBucket: 'codetut-io.appspot.com',
    messagingSenderId: '305752541087'
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
