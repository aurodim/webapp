import { Component } from '@angular/core';
import { HostListener } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { Router, Event, NavigationStart, NavigationEnd } from '@angular/router';
import * as M from 'materialize-css/dist/js/materialize';
import { Section } from 'src/app/bits/bit.model';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  title = '< CodeTut.io /> ';
  collapsed = false;
  sectionY = 0;
  postSections: Section[] = [];
  loggedIn = false;

  constructor(private router: Router, public afAuth: AngularFireAuth, public db: AngularFirestore) {}

  ngAfterViewInit() {
    this.router.events.subscribe((event: Event) => {
        if (event instanceof NavigationStart) {
          if (event.url === '/') {
            this.collapsed = false;
            document.getElementsByTagName('nav')[0].removeAttribute('collapse');
          } else {
            document.getElementsByTagName('nav')[0].setAttribute('collapse', 'true');
            if (document.getElementsByClassName('feed-container').length > 0) {
              document.getElementsByClassName('feed-container')[0].setAttribute('collapse', 'true');
            }
          }
        }

        if (event instanceof NavigationEnd) {
          if (event.url === '/') {
            if (document.getElementsByClassName('feed-container').length > 0) {
              document.getElementsByClassName('feed-container')[0].removeAttribute('collapse');
            }
        } else {
            this.collapsed = true;
          }
        }
    });

    const elems = document.querySelectorAll('.fixed-action-btn');
    const instances = M.FloatingActionButton.init(elems, {
      hoverEnabled: false,
      direction: 'right'
    });

    const elemsCC = document.querySelectorAll('input.validate, textarea.materialize-textarea');
    M.CharacterCounter.init(elemsCC);

    const elemsDD = document.querySelectorAll('.dropdown-trigger');
    const instancesDD = M.Dropdown.init(elemsDD, {
      hover: true,
      constrainWidth: false
    });
  }

  onMenuOpen() {
    const elems = document.querySelectorAll('.sidenav');
    const instances = M.Sidenav.init(elems, {});
    const instance = M.Sidenav.getInstance(elems[0]);
    instance.open();
  }

  onOpen() {
    document.getElementsByTagName('nav')[0].setAttribute('collapse', 'true');
    document.getElementsByClassName('feed-container')[0].setAttribute('collapse', 'true');
    setTimeout(() => this.collapsed = true, 300);
  }

  onSectionAdd() {
    this.postSections.push(new Section('', '', ''));
  }

  onLogin(data) {
    this.afAuth.auth.signInWithEmailAndPassword(data.email, data.password).then((res) => {
      this.loggedIn = true;
      const toastHTML = '<span>Logged In</span>';
      M.toast({html: toastHTML});
    }).catch(error => {
      const toastHTML = '<span class="white-text">Login Failed</span>';
      M.toast({html: toastHTML, classes: 'red'});
    });
  }

  onPost(data) {
    const postSections = this.postSections.map((section) => Object.assign({}, section));
    this.db.collection('bits').doc(data.id).set({
      id: data.id,
      title: data.title,
      description: data.description,
      attributes: {
        image: data.id,
        iframe: `https://repl.it/@aurodim/${data.rid}?lite=true`,
        date: new Date()
      },
      sections: postSections
    }).then(res => {
      const toastHTML = '<span>Success! Logged Out</span>';
      M.toast({html: toastHTML});
      this.afAuth.auth.signOut();
    }).catch(error => {
      console.log(error);
      const toastHTML = '<span class="white-text">Failed to Post</span>';
      M.toast({html: toastHTML, classes: 'red'});
    });
  }

  onCodeAdd($event, i: number) {
    this.postSections[i].code = $event.srcElement.value.replace(/\s+/g, '');
  }

  onBack() {
    this.collapsed = false;
    document.getElementsByTagName('nav')[0].removeAttribute('collapse');
    setTimeout(() => {
      document.getElementsByClassName('feed-container')[0].removeAttribute('collapse');
    }, 700);
  }
}
