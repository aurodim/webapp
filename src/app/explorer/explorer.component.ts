import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Bit } from '../bits/bit.model';
import { BitsService } from '../bits/bits.service';
import * as beautifyJS from 'js-beautify';
import { DomSanitizer } from '@angular/platform-browser';
import * as M from 'materialize-css/dist/js/materialize';

@Component({
  selector: 'app-explorer',
  templateUrl: './explorer.component.html',
  styleUrls: ['./explorer.component.scss']
})
export class ExplorerComponent implements OnInit, AfterViewInit {
  loaded = false;
  bit: Bit;

  constructor(private route: ActivatedRoute, private router: Router, private bitsService: BitsService, private sanitizer: DomSanitizer) {

  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      if (this.bitsService.getBits().length === 0) {
        this.bitsService.db.collection('bits').doc(params.id).get().subscribe((bit) => {
          if (bit.exists) {
            const data = bit.data();
            this.bit = new Bit(data.id, data.title, data.description, data.attributes, data.sections, this.sanitizer);
            this.loaded = true;
          } else {
            this.redirect();
          }
        });
      } else {
        this.bit = this.bitsService.getBit(params.id);
        if (!this.bit) {
          return this.redirect();
        }
        this.loaded = true;
      }
    });
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const elems = document.querySelectorAll('.tooltipped');
      const instances = M.Tooltip.init(elems, {});
      console.log(elems);
    }, 2000);
  }

  redirect() {
    this.router.navigate(['/not-found']);
  }
}
