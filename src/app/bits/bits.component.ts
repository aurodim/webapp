import { Component, AfterViewInit } from '@angular/core';
import { BitsService } from './bits.service';
import { Bit } from './bit.model';

@Component({
  selector: 'app-bits',
  templateUrl: './bits.component.html',
  styleUrls: ['./bits.component.scss']
})
export class BitsComponent implements AfterViewInit {
  loaded = false;
  bits: Bit[];
  pagerIndex = 1;

  constructor(private bitsService: BitsService) {

  }

  ngAfterViewInit() {
    this.bitsService.db.collection('bits').valueChanges().subscribe((bits) => {
      this.bitsService.setBits(<Bit[]>bits);
      this.bits = this.bitsService.getBits();
      this.loaded = true;
    });
  }

  onPageChange($event) {
    this.pagerIndex = $event;
  }

}
