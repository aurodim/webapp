import { Injectable, EventEmitter } from '@angular/core';
import { Bit } from 'src/app/bits/bit.model';
import BitsData from './bits.data';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class BitsService {
  bits: Bit[] = [];

  constructor(public db: AngularFirestore, private sanitizer: DomSanitizer) {}

  setBits(bits: Bit[]) {
    this.bits = bits.map((bit) => new Bit(bit.id, bit.title, bit.description, bit.attributes, bit.sections, this.sanitizer));
  }

  getBit(id: string) {
    return this.bits.slice().filter((bit) => bit.id === id).length > 0 ? this.bits.slice().filter((bit) => bit.id === id)[0] : null;
  }

  getBits() {
    return this.bits.slice().reverse();
  }
}
