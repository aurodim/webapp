import { TestBed, inject } from '@angular/core/testing';

import { BitsService } from './bits.service';

describe('BitsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BitsService]
    });
  });

  it('should be created', inject([BitsService], (service: BitsService) => {
    expect(service).toBeTruthy();
  }));
});
