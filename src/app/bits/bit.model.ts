import * as beautifyJS from 'js-beautify';
import { DomSanitizer } from '@angular/platform-browser';
import { Injectable } from '@angular/core';

@Injectable()
export class Bit {
  constructor(public id, public title: string, public description: string, public attributes: Attributes, public sections: Section[],
    private _sanitizer?: DomSanitizer) {
    this.attributes = new Attributes(attributes.image, attributes.iframe, attributes.date, _sanitizer);
    this.sections = this.sections.map((section) => new Section(section.title, section.code, section.text));
  }
}

class Attributes {
  constructor(public image: string, public iframe: any, public date, private sanitizer: DomSanitizer) {
    this.iframe = sanitizer.bypassSecurityTrustResourceUrl(iframe);
    this.date = new Date(1970, 0, 1).setSeconds(date.seconds);
  }
}

export class Section {

  constructor(public title: string, public code: string, public text: string) {
    this.code = beautifyJS(code, { indent_size: 2, space_in_empty_paren: true });
    this.text = this.text.replace(/(<var>)(.*?)(<\/var>)/gi, '<i class="primary-text">$2</i>');
    this.text = this.text.replace(/(<func>)(.*?)(<\/func>)/gi, '<i class="orange-text">$2</i>');
  }
}
