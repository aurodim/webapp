const BitsData: any = [
  {
    'id' : 'power-of',
    'title' : 'To Power Of X',
    'description' : 'JavaScript arrow function syntax that returns any number to the power of the given positive parameter;',
    'attributes' : {
      'image' : 'https://res.cloudinary.com/aurodim/image/upload/v1548988741/codetut.io/to_power_of.png',
      'iframe' : 'https://repl.it/@aurodim/ToPowerOf?lite=true',
      'date' : '01/30/2019',
    },
    'sections' : [
      {
        'title' : 'const calcExponent()',
        'code' : `const calcExponent = (x, y) => {
  if (x > 0 && y > 0) {
    return Math.pow(x, y);
  } else {
    return 'Intergers must be positive numbers!';
  }
};`,
        'text' : `The constant function <i class="orange-text">calcExponent</i> takes two parameters <i class="primary-text">x</i> and <i class="primary-text">y</i>;
          <br/><br/>The function will test if <i class="primary-text">x, y</i> are positive numbers;
          <br/><br/>If <i class="primary-text">x</i> and <i class="primary-text">y</i> are both positive numbers, it will return <i class="primary-text">x</i> to the power of <i class="primary-text">y</i>
           using JavaScript's built-in object method Math.pow();`
      },
      {
        'title' : 'var result',
        'code' : `var result = calcExponent(2, 10);`,
        'text' : `The variable <i class="primary-text">result</i> will now hold the value that our previously defined function, <i class="orange-text">calcExponent</i>, returns;`
      },
      {
        'title' : 'console.log()',
        'code' : `console.log(result);`,
        'text' : `The value of our variable <i class="primary-text">result</i> will be logged to the system's console;`
      }
    ]
  },
  {
    'id' : 'sum_of',
    'title' : 'Sum of X and Y',
    'description' : 'JavaScript arrow function syntax that returns the sum of any two numbers that are passed as parameters',
    'attributes' : {
      'image': 'https://res.cloudinary.com/aurodim/image/upload/v1548988741/codetut.io/sum_of_num.png',
      'iframe' : 'https://repl.it/@aurodim/SumOfXY?lite=true',
      'date' : '2/1/2019',
    },
    'sections' : [
      {
        'title' : 'const SumOfNum',
        'code' : `const sumOfNum = (x, y) => {
  return x + y;
};`,
        'text' : `The constant function <i class="orange-text">sumOfNum</i> takes two parameters <i class="primary-text">x</i> and <i class="primary-text">y</i>;
          <br/><br/>The function will return the sum of <i class="primary-text">x</i>, the first addend, and <i class="primary-text">y</i>, the second addend;`
      },
      {
        'title' : 'var result',
        'code' : `var result = sumOfNum(7, 6);`,
        'text' : `The variable <i class="primary-text">result</i> will now hold the value that our previously defined function, <i class="orange-text">sumOfNum</i>, returns;`
      },
      {
        'title' : 'console.log()',
        'code' : `console.log(result);`,
        'text' : `The value of our variable <i class="primary-text">result</i> will be logged to the system's console;`
      }
    ]
  },
  {
    'id' : 'odd_or_even',
    'title' : 'Is X Odd or Even?',
    'description' : 'JavaScript arrow function syntax that returns whether the passed parameter is a odd or even number.',
    'attributes' : {
      'image': 'https://res.cloudinary.com/aurodim/image/upload/v1548988741/codetut.io/odd_or_even_number.png',
      'iframe' : 'https://repl.it/@aurodim/OddOrEven?lite=true',
      'date' : '1/31/2019',
    },
    'sections' : [
      {
        'title' : 'var list',
        'code' : `var list = [5, 3, 7, 2, 6, 8, 1, 4];`,
        'text' : `The variable <i class="orange-text">list</i> defines a new <i class="orange-text">Array</i> with 8 items.`
      },
      {
        'title' : 'Array.min = function(list)',
        'code' : `Array.min = function(list) {
  return Math.min.apply(Math, list);
};`,
        'text' : `The variable <i class="primary-text">result</i> will now hold the value that our previously defined function, <i class="orange-text">sumOfNum</i>, returns;`
      },
      {
        'title' : 'console.log()',
        'code' : `console.log(result);`,
        'text' : `The value of our variable <i class="primary-text">result</i> will be logged to the system's console;`
      }
    ]
  }
];

export default BitsData;

/*

{
  'title' : '',
  'code' : ``,
  'text' : ``
}

*/
