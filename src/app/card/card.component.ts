import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Bit } from '../bits/bit.model';
import { Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() bit: Bit;

  constructor() { }

  ngOnInit() {

  }

}
