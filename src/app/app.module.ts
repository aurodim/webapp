import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BitsComponent } from './bits/bits.component';
import { BitsService } from './bits/bits.service';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';

import { CookieLawModule } from 'angular2-cookie-law';

import { Cloudinary } from 'cloudinary-core';
import { CloudinaryModule } from '@cloudinary/angular-5.x';

export const cloudinaryLib = {
  Cloudinary: Cloudinary
};
/*
  IMPORT MODULES WE NEED FROM NGX-MATERIALIZE
*/
import { MzNavbarModule } from 'ngx-materialize';
import { MzPaginationModule } from 'ngx-materialize';
import { MzSpinnerModule } from 'ngx-materialize';
import { MzCardModule } from 'ngx-materialize';
import { CardComponent } from './card/card.component';
import { MzIconModule, MzIconMdiModule } from 'ngx-materialize';
import { ExplorerComponent } from './explorer/explorer.component';
import { MzProgressModule } from 'ngx-materialize';
import { MzModalModule } from 'ngx-materialize';
import { MzTooltipModule } from 'ngx-materialize';
import { MzFeatureDiscoveryModule } from 'ngx-materialize';
import { MzButtonModule } from 'ngx-materialize';
import { AboutComponent } from './about/about.component';
import { PartnersComponent } from './partners/partners.component';
import { ContactComponent } from './contact/contact.component';
import { LegalComponent } from './legal/legal.component';
import { UpcomingComponent } from './upcoming/upcoming.component';
import { MzCollapsibleModule } from 'ngx-materialize';
import { MzSidenavModule } from 'ngx-materialize';
import { MzDropdownModule } from 'ngx-materialize';
import { MzInputModule } from 'ngx-materialize';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { CookiesPolicyComponent } from './cookies-policy/cookies-policy.component';

const appRoutes: Routes = [
  { path: 'not-found', component: PageNotFoundComponent },
  { path: 'about', component: AboutComponent },
  { path: 'upcoming', component: UpcomingComponent },
  { path: 'partners', component: PartnersComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'legal', children: [
    { path: 'terms-and-conditions', component: TermsAndConditionsComponent },
    { path: 'privacy-policy', component: PrivacyPolicyComponent },
    { path: 'cookies-policy', component: CookiesPolicyComponent },
    { path: '', redirectTo: '/not-found', pathMatch: 'full' }
  ]},
  { path: ':id',      component: ExplorerComponent },
  { path: '',
    component: BitsComponent,
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    ExplorerComponent,
    BitsComponent,
    PageNotFoundComponent,
    AboutComponent,
    PartnersComponent,
    ContactComponent,
    UpcomingComponent,
    LegalComponent,
    PrivacyPolicyComponent,
    TermsAndConditionsComponent,
    CookiesPolicyComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'codetut-io'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    CloudinaryModule.forRoot(cloudinaryLib, {
      cloud_name: 'aurodim'
    }),
    MzNavbarModule,
    MzPaginationModule,
    MzSpinnerModule,
    MzCardModule,
    MzIconModule,
    MzIconMdiModule,
    MzProgressModule,
    MzModalModule,
    MzTooltipModule,
    MzFeatureDiscoveryModule,
    MzButtonModule,
    MzCollapsibleModule,
    MzDropdownModule,
    MzInputModule,
    FormsModule,
    CookieLawModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  providers: [
    BitsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
