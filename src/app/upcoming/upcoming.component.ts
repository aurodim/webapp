import { Component, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.scss']
})
export class UpcomingComponent implements AfterViewInit {
  loaded = false;

  constructor() { }

  ngAfterViewInit() {
    setTimeout(() => {
      this.loaded = true;
    }, 1000);
  }

}
